//
//  RecordsServiceTest.swift
//  iTranslateSample01Tests
//
//  Created by Lazar Jovicic on 19/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import XCTest
@testable import iTranslateSample01


class MockRepository : RecordRepository {
    func saveRecord(_ record: Record) {
        
    }
    
    func getAllRecords() -> [Record] {
        return [Record(withFileName: "Recording123.m4u")]
    }
    
    func deleteRecord(_ record: Record) -> Bool {
        if record.name == "Recording123.m4u" {
            return true
        } else {
            return false
        }
    }
}

class RecordsServiceTest: XCTestCase {
    var service : RecordsService!
    override func setUp() {
        service = RecordsServiceImpl(withRepository: MockRepository())
    }
    
    
    func testGetAllRecordsReturnsRecordsArray() {
        let records = service.getAllRecords()
        XCTAssert(records.count == 1)
    }
    
    func testDeleteRecordReturnsTrue() {
        let isRecordDelete = service.deleteRecord(Record(withFileName: "Recording123.m4u"))
        XCTAssertTrue(isRecordDelete)
    }
    
    func testDeleteRecordForGivenRecordReturnsFalse() {
        let isRecordDelete = service.deleteRecord(Record(withFileName: "Recording0000.m4u"))
        XCTAssertFalse(isRecordDelete)
    }
}
