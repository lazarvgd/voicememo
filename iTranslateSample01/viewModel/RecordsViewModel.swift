//
//  RecordsViewModel.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 18/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

class RecordsViewModel : NSObject {
    private let recordsService = RecordsServiceImpl(withRepository: RecordRepositoryImpl())
    private let audioPlayer = AudioPlayerService()
    @objc dynamic var records : [Record]!
    @objc dynamic var hasError = false
    @objc dynamic var isDeleted = false
    
    func getRecordsCount() -> Int {
        records = recordsService.getAllRecords()
        return records.count
    }
    
    func getRecordDuration(forRow index: Int) -> String {
        let record = records[index]
        let durationString = audioPlayer.getDurationString(for:record)
        return durationString
    }
    
    func play(recordForRow index:Int) {
        do {
            try audioPlayer.play(records[index])
        } catch {
            hasError = true
        }
    }
    
    func deleteRecord(forRow index: Int) {
        isDeleted = recordsService.deleteRecord(records[index])
        records.remove(at: index)
    }
    
    func stopAudioPlayer() {
        audioPlayer.stop()
    }
}
