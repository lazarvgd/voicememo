//
//  MainViewModel.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 16/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

class MainViewModel : NSObject {
    private let recorder = AudioRecorderService(withRepository: RecordRepositoryImpl())
    @objc dynamic var hasError = false
    
    func startRecording() {
        do {
        try recorder.startRecording()
        } catch {
            hasError = true
        }
    }
    
    func stopRecording() {
        recorder.stopRecording()
    }
}
