//
//  ViewController.swift
//  iTranslateSample01
//
//  Created by Andreas Gruber on 04.07.19.
//  Copyright © 2019 iTranslate. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class MainViewController: UIViewController {
    var errorObserver : NSKeyValueObservation!
    let viewModel = MainViewModel()
    var isRecording = false
    let lightBlueColor = UIColor(red: 67/255, green: 155/255, blue: 214/255, alpha: 1.0)
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var showRecordsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        setupShowRecordsButton()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObserver()
    }
    private func setupShowRecordsButton() {
        showRecordsButton.backgroundColor = .clear
        showRecordsButton.layer.cornerRadius = 10
        showRecordsButton.layer.borderWidth = 1
        showRecordsButton.layer.borderColor = lightBlueColor.cgColor
    }
    
    @IBAction func showRecordsScreen(_ sender: UIButton) {
        if !isRecording {
            let navController = storyboard?.instantiateViewController(withIdentifier: "RecordsNavController") as! UINavigationController
            navController.modalPresentationStyle = .fullScreen
            present(navController, animated: true)
        } else {
            displayAlert(title: "Recording in progress", message: "Please finish recording in order to access all recordings.")
        }
        
    }
    
    private func setupObserver() {
          errorObserver = viewModel.observe(\MainViewModel.hasError, options: [.new], changeHandler: { (vm:MainViewModel, change:NSKeyValueObservedChange<Bool>) in
            DispatchQueue.main.async {
               if let hasError = change.newValue {
                    if hasError {
                        self.displayAlert(title: "Ooops!", message: "Something went wrong, please try again.")
                        self.isRecording = false
                        self.recordButton.tintColor = self.lightBlueColor
                        self.viewModel.stopRecording()
                    }
                }
            }
        })
    }
    
    fileprivate func controlRecorder() {
        if isRecording {
            isRecording = false
            recordButton.tintColor = lightBlueColor
            viewModel.stopRecording()
        } else {
            isRecording = true
            recordButton.tintColor = .red
            viewModel.startRecording()
        }
    }
    
    fileprivate func openMicrophonePermissionScreen() {
        let micViewContoller = storyboard?.instantiateViewController(withIdentifier: "MicrophoneViewController") as! MicrophoneViewController
        micViewContoller.delegate = self
        micViewContoller.modalPresentationStyle = .fullScreen
        present(micViewContoller, animated: true) {
            self.checkMicrophonePermissions()
        }
    }
    
    fileprivate func checkMicrophonePermissions() {
        let authorizationStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        if authorizationStatus == .authorized {
            controlRecorder()
        } else {
            openMicrophonePermissionScreen()
        }
    }
    
    @IBAction func record(_ sender: UIButton) {
        checkMicrophonePermissions()
    }
    
    private func displayAlert(title:String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true,completion: nil)
    }
    
    private func setupButton() {
        let origImage = UIImage(named: "mic.png")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        recordButton.setImage(tintedImage, for: .normal)
        recordButton.tintColor = lightBlueColor
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        errorObserver.invalidate()
    }
}

extension MainViewController : MainViewControllerDelegate {
    func executeOnDismiss() {
        self.controlRecorder()
    }
}
