//
//  MicrophoneViewController.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 18/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import UIKit
import AVFoundation

protocol MainViewControllerDelegate : AnyObject {
    func executeOnDismiss()
}

class MicrophoneViewController: UIViewController {
    weak var delegate : MainViewControllerDelegate!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            self.containerView.layer.cornerRadius = 10
            self.containerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var microphoneImage: UIImageView!
    @IBOutlet weak var allowButtonOutlet: UIButton!
    let lightBlueColor = UIColor(red: 67/255, green: 155/255, blue: 214/255, alpha: 1.0)
    let lightBlueGrayColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        microphoneImage.image = microphoneImage.image?.withRenderingMode(.alwaysTemplate)
        microphoneImage.tintColor = lightBlueGrayColor
        
        allowButtonOutlet.backgroundColor = .clear
        allowButtonOutlet.layer.cornerRadius = 10
        allowButtonOutlet.layer.borderWidth = 1
        allowButtonOutlet.layer.borderColor = lightBlueColor.cgColor
    }
    
    @IBAction func allowButtonPressed(_ sender: UIButton) {
        AVAudioSession.sharedInstance().requestRecordPermission { (isAccepted) in
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    self.delegate?.executeOnDismiss()
                }
            }
        }
    }
    
    @IBAction func maybeLaterButtonPressed(_ sender: UIButton) {
        print("DISMISS")
        self.dismiss(animated: true, completion: nil)
    }
    
}
