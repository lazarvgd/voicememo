//
//  RecorsViewController.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 16/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import UIKit
import AVFoundation
import SwipeCellKit

class RecorsViewController: UIViewController {
    var recordsNumber : Int = 0
    var errorObserver : NSKeyValueObservation!
    var recordsObserver : NSKeyValueObservation!
    var deleteObserver : NSKeyValueObservation!
    let viewModel = RecordsViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = true
        tableView.register(UINib(nibName: "RecordTableViewCell", bundle: nil), forCellReuseIdentifier: "RecordCell")
        recordsNumber = viewModel.getRecordsCount()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupObservers()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        viewModel.stopAudioPlayer()
        dismiss(animated: true, completion: nil)
    }
    
    private func setupObservers() {
        errorObserver = viewModel.observe(\RecordsViewModel.hasError, options: [.new], changeHandler: { (vm:RecordsViewModel, change:NSKeyValueObservedChange<Bool>) in
            DispatchQueue.main.async {
                if let hasError = change.newValue {
                    if hasError {
                        self.displayAlert(title: "Ooops!", message: "Something went wrong, please try again.")
                    }
                }
            }
        })
        
        recordsObserver = viewModel.observe(\RecordsViewModel.records, options: [.new], changeHandler: { (vm:RecordsViewModel, change:NSKeyValueObservedChange<[Record]>) in
            if let recs = change.newValue {
                self.recordsNumber = recs.count
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
        
        deleteObserver = viewModel.observe(\RecordsViewModel.isDeleted, options: [.new], changeHandler: { [weak self] (vm:RecordsViewModel, change:NSKeyValueObservedChange<Bool>) in
            if let isDeleted = change.newValue {
                if isDeleted {
                    self?.displayAlert(title: "Delete Successfull", message: "Record deleted.")
                } else {
                    self?.displayAlert(title: "Delete Unsuccessfull", message: "Error occurred during deletition. \nTry again.")
                }
            }
        })
    }
    
    private func displayAlert(title:String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true,completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        errorObserver.invalidate()
        recordsObserver.invalidate()
        deleteObserver.invalidate()
    }
    
}

extension RecorsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.play(recordForRow: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}

extension RecorsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsNumber
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecordCell", for: indexPath) as! RecordTableViewCell
        cell.delegate = self
        cell.recordingNameLabel.text = "Recording \(indexPath.row + 1)"
        cell.recordingDurationLabel.text = viewModel.getRecordDuration(forRow: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Recently used"
    }
    
}

extension RecorsViewController : SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { [weak self] action, indexPath in
            self?.viewModel.deleteRecord(forRow: indexPath.row)
        }
        
        deleteAction.image = UIImage(named: "delete")
        return [deleteAction]
    }
}
