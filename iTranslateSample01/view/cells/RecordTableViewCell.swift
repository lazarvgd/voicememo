//
//  RecordTableViewCell.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 17/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import UIKit
import SwipeCellKit

class RecordTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var recordingDurationLabel: UILabel!
    @IBOutlet weak var recordingNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
