//
//  RecordService.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 17/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

protocol RecordsService {
    func getAllRecords() -> [Record]
    func deleteRecord(_ rec: Record) -> Bool
}
