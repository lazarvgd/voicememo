//
//  PlayerService.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 18/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation
import AVFoundation

class AudioRecorderService : NSObject, AVAudioRecorderDelegate{
    var audioRecorder : AVAudioRecorder!
    let recordRepo : RecordRepository
    var fileName : String!
    
    init(withRepository repo:RecordRepository) {
        self.recordRepo = repo
    }
    
    func startRecording() throws {
        if audioRecorder == nil {
            let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let path = paths[0]
            fileName = "Recording \(Date()).m4a"
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVSampleRateKey:32000, AVNumberOfChannelsKey: 1, AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue]
            do {
                audioRecorder = try AVAudioRecorder(url: path.appendingPathComponent(fileName), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.record()
            } catch {
                throw RecError.RecorderError(withMessage: "unable to start recorder")
            }
        }
    }
    
    func stopRecording() {
        if audioRecorder != nil {
            audioRecorder.stop()
            audioRecorder = nil
            recordRepo.saveRecord(Record(withFileName: fileName))
        }
    }
}
