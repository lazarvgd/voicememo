//
//  RecordServiceImpl.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 17/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

class RecordsServiceImpl : RecordsService {
    
    let repository : RecordRepository
    
    init(withRepository repo : RecordRepository) {
        self.repository = repo
    }
    
    func getAllRecords() -> [Record] {
        return repository.getAllRecords()
    }
    
    func deleteRecord(_ rec: Record) -> Bool {
       return repository.deleteRecord(rec)
    }
}
