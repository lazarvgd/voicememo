//
//  AudioPlayerService.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 18/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayerService {
    var audioPlayer : AVAudioPlayer!
    
    func play(_ record:Record) throws {
        do {
            let path = getDir().appendingPathComponent(record.name)
            audioPlayer = try AVAudioPlayer(contentsOf: path)
            audioPlayer.play()
        } catch {
            throw PlayerError.ReprodError(withMessage: "Unable to play record")
        }
    }
    
    func stop() {
        if let player =  audioPlayer {
            player.stop()
        }
        
    }
    
    private func getDir() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDir = paths[0]
        return documentDir
    }
    
    func getDurationString(for rec: Record) -> String {
        let asset = AVURLAsset(url: getDir().appendingPathComponent(rec.name), options: nil)
        let audioDuration = asset.duration
        let durationInSec = Int(CMTimeGetSeconds(audioDuration))
        let mins = Int(durationInSec/60)
        let sec = (durationInSec - mins * 60)
        let formatedString = formatNumberToString(firstNumber: mins, secondNumber: sec)
        return formatedString
    }
    
    private func formatNumberToString(firstNumber:Int, secondNumber:Int) -> String{
        let str1 = String(format:"%02d", firstNumber)
        let str2 = String(format:"%02d", secondNumber)
        return str1 + ":" + str2
    }
}
