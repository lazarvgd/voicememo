//
//  Record.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 16/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation
import RealmSwift

class Record : Object {
    @objc dynamic var name: String = ""
    
    convenience init(withFileName name : String) {
        self.init()
        self.name = name
    }
}
