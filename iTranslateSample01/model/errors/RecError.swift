//
//  RecError.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 18/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

enum RecError : Error {
    case RecorderError(withMessage:String)
}

enum PlayerError : Error {
    case ReprodError(withMessage:String)
}
