//
//  RecordRepo.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 17/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation

protocol RecordRepository {

    func saveRecord(_ record: Record)
    func getAllRecords() -> [Record]
    func deleteRecord(_ record:Record) -> Bool
}
