//
//  RecordService.swift
//  iTranslateSample01
//
//  Created by Lazar Jovicic on 16/09/2020.
//  Copyright © 2020 iTranslate. All rights reserved.
//

import Foundation
import AVFoundation
import RealmSwift

class RecordRepositoryImpl : RecordRepository {
    
    let realm : Realm = try! Realm()
    
    func saveRecord(_ record: Record) {
        do {
            try realm.write {
                realm.add(record)
            }
            
        } catch let error as NSError {
            print("error witing to realm for object count \(error)")
        }
    }
    
    func getAllRecords() -> [Record] {
            let records = realm.objects(Record.self)
            let converted = records.reduce(List<Record>()) { (list, element) -> List<Record> in
                list.append(element)
                return list
            }
            return Array(converted)
    }
    
    func deleteRecord(_ record:Record) -> Bool {
        do {
            try realm.write {
                realm.delete(realm.objects(Record.self).filter("name=%@",record.name))
            }
            return true
        } catch {
            print("error reding from realm for object count: \(error)")
            return false
        }
    }
}
