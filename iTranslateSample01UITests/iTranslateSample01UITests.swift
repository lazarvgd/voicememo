//
//  iTranslateSample01UITests.swift
//  iTranslateSample01UITests
//
//  Created by Andreas Gruber on 04.07.19.
//  Copyright © 2019 iTranslate. All rights reserved.
//

import XCTest

class iTranslateSample01UITests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
    }
    
    func test01OpenMicrophoneControllerAndPressMaybeLaterButton() {
        
        let app = XCUIApplication()
        app.otherElements.containing(.button, identifier:"Show Records").children(matching: .button).element(boundBy: 0).tap()
        XCTAssertTrue(app.buttons["Maybe later"].exists)
        app.buttons["Maybe later"].tap()
        XCTAssertTrue(app.buttons["Show Records"].exists)
    }
    
    func test02NoRecords() {
        
        let app = XCUIApplication()
        app.buttons["Show Records"].tap()
        XCTAssertFalse(XCUIApplication().tables/*@START_MENU_TOKEN@*/.staticTexts["Recording 1"]/*[[".cells.staticTexts[\"Recording 1\"]",".staticTexts[\"Recording 1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.exists)
    }
    
    func test03OpenMicrophoneControllerAndPressAllowButton() {
        
        let app = XCUIApplication()
        app.buttons["mic"].tap()
        XCTAssertTrue(app.buttons["Allow"].exists)
        app.buttons["Allow"].tap()
        addUIInterruptionMonitor(withDescription: "Access to sound recording") { (alert) -> Bool in
            if alert.staticTexts["\"iTranslateSample01\" would like to use your microphone for recording your sound."].exists {
                alert.buttons["Don’t Allow"].tap()
            } else {
                alert.buttons["OK"].tap()
            }
            return true
        }
        app.tap()
        app.buttons["mic"].tap()
        XCTAssertTrue(app.buttons["Show Records"].exists)
        
    }
    
    
    func test04RecordAudio() {
        let app = XCUIApplication()
        let micButton = app.buttons["mic"]
        micButton.tap()
        sleep(5)
        micButton.tap()
        app.buttons["Show Records"].tap()
        XCTAssertTrue(app.tables.staticTexts["Recording 2"].exists)
    }
    
    func test05PlayRecord() {
        let app = XCUIApplication()
        app/*@START_MENU_TOKEN@*/.staticTexts["Show Records"]/*[[".buttons[\"Show Records\"].staticTexts[\"Show Records\"]",".staticTexts[\"Show Records\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Recording 2"]/*[[".cells.staticTexts[\"Recording 2\"]",".staticTexts[\"Recording 2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(2)
        print("************************************* \(app.alerts.description)")
        XCTAssertFalse(app.alerts.element.staticTexts["Ooops!"].exists)
        app.navigationBars["Recordings"].buttons["Done"].tap()
    }
    
    func test06RemoveRecord2() {
        
        let app = XCUIApplication()
        app.buttons["Show Records"].tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Recording 2"]/*[[".cells.staticTexts[\"Recording 2\"]",".staticTexts[\"Recording 2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeLeft()
        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Delete"]/*[[".cells.buttons[\"Delete\"]",".buttons[\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertFalse(app.tables.staticTexts["Recording 2"].exists)
    }
}
